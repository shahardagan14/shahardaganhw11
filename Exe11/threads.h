#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>    
#include <time.h>
using namespace std;


void I_Love_Threads();// print i love thread
void call_I_Love_Threads();// call the function I love threads using thread 
void printVector(vector<int> primes); //print the vector 
void getPrimes(int begin, int end, vector<int>& primes);//get the primes numbers and added them to the vector 
vector<int> callGetPrimes(int begin, int end);// call the function get primes
void writePrimesToFile(int begin, int end, ofstream& file);//write all prime numbers between the range(begin- end) to the file
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N); // the function write to the file path the prime numbers in the range of begin - end
