#include "threads.h"

int main()
{
	call_I_Love_Threads();
	//create 3 vectors 
	vector<int> primes1 = callGetPrimes(0, 1000);
	vector<int> primes2 = callGetPrimes(0, 100000);
	vector<int> primes3 = callGetPrimes(0, 1000000);
	
	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 3);
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 3);
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 3);

	system("pause");
	return 0;
}