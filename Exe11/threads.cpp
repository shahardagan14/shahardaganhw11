#include "threads.h"

void I_Love_Threads()
{
	//print output
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	std::thread thread(I_Love_Threads);     // spawn new thread that calls I love thread()
	thread.join();
}


void printVector(vector<int> primes)
{
	//print the prime numbers vector 
	for (int i = 0; i < primes.size(); i++)
	{
		std::cout << primes[i] << endl;
	}
}

void getPrimes(int begin, int end, std::vector<int>& primes) 
{
	bool flag = true;
	for (int i =  begin; i < end; i++)
	{
		for (int j = 2; j < sqrt(i) && flag ; j++)
		{
			//check if the number is prime or not
			if ( i % j == 0)
			{
				flag = false;
				break;
			}
		}
		//if the flag still true so the number is prime
		if (flag&& i != 2)
		{
			primes.push_back(i);
		}
		flag = true;
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	//create a new vector and call the function to get the primes number using thread 
	vector <int> vect = vector<int>();
	thread t(getPrimes, begin, end,std::ref(vect));
	t.join();
	return vect;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	//get the prime nunmbers first
	vector<int> result = callGetPrimes(begin, end);
	string temp;
	//write each one in to the file
	for (int i = 0; i < result.size(); i++)
	{
		file<< result[i] << endl;
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	vector<thread>threads = vector<thread>();
	int dist = (end - begin) / N;
	//open the file 
	ofstream file;
	file.open(filePath);
	if (file.is_open())
	{
		//get the begining time 
		auto start = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < N; i++)
		{
			// check if the begin is greater than the limit 
			if (begin + (i + 1)*dist >= end)
			{
				break;
			}
			else
			{
				//otherwise call the thread to write to the file				
				threads.push_back(thread(writePrimesToFile, begin + i * dist, begin + (i + 1) *dist, ::ref(file)));
			}
		}
		//the finish time of the thread 
		for (auto it = threads.begin(); it != threads.end(); it++) 
		{
			it->join();
		}

		auto finish = std::chrono::high_resolution_clock::now();
		//the diffrence between the end time and the begining
		std::chrono::duration<double> elapsed = finish - start;
		file.close();//close the file
		cout << "The time of the thread is " << elapsed.count()<<endl;
	}
	else 
	{
		cout << "Exception while opening the file " << endl;
	}
}
